# Python for Machine Learning

## Description:

This project contains a python workshop to test the basic notions of the language for machine learning use cases.
The workshop to complete is directly explained on the jupyter notebook.
You will find on this project the necessary data to complete the workshop.

### Project organization:

* tp_stage.ipynb: a jupyter notebook with the workshop (recommended)
* tp_stage.py: the equivalent workshop python script

### Important notes : 

The used environment is Jupyter (tp_stage.ipynb), please set all the folders and file on the same level with file data.pickle .
For the 3D plot, matplotlib widget supported by Jupyter is implemented for an interactive design. If the required libraries are missing, please revert to inline mode. 
Tensorflow backend is used for keras libraries, in the MLP step. Another possible way is to revert back to MLPClassifier in Sklearn.neural_network. 
A CSS file is included in the static area. 

